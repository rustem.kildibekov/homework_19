#include <iostream>

using namespace std;

class Animal
{
protected:
	string _type;
public:
	Animal() {};
	Animal(string type) : _type(type) {
		cout << "Create a " + _type << endl;
	};
	~Animal() {
		cout << "Delete a " + _type << endl;
	};

	virtual void voice() {};
};

class Dog : public Animal
{
public:
	Dog() : Animal("dog") {
	};
	~Dog() {
	};

	void voice() override {
		std::cout << "Woof!" << std::endl;
	};
};

class Cat : public Animal
{
public:
	Cat() : Animal("cat") {
	};
	~Cat() {
	};

	void voice() override {
		std::cout << "Meow!" << std::endl;
	};
};

class Cow : public Animal
{
public:
	Cow() : Animal("cow") {
	};
	~Cow() {
	};

	void voice() override {
		std::cout << "Mooo!" << std::endl;
	};
};

int main() {

	Animal*  animals[3];
	animals[0] = new Cat();
	animals[1] = new Dog();
	animals[2] = new Cow();

	for (Animal* a : animals) {
		a->voice();
	}

	for (Animal* a : animals) {
		delete a;
	}


}
